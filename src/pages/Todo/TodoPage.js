import React, { useState, useEffect } from 'react';
import UserInfo from './UserInfo';
import TodoList from './TodoList';
import { Row, Col } from 'antd';
import Header from '../../component/Header';

const TodoPage = (props) => {
    const userId = props.match.params.user_id;
    const [user, setUser] = useState({});
    const [todoList, setTodoList] = useState([]);
    const [selectedFilter, setSelectedFilter] = useState("All");

    useEffect(() => {
        fetchUserdata();
        fetchTododata();
    }, [])

    const fetchUserdata = () => {
        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data);
            })
            .catch(error => console.log(error));
    }

    const fetchTododata = () => {
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setTodoList(data);
            })
            .catch(error => console.log(error));
    }

    return (
        <div style={{backgroundColor: "#EEEEEE"}}>
            <Row type="flex" justify="center">
                <Col span={20} style={{ marginTop: 30 }}>
                    <Header
                        title="Todo List"
                        onBackGoto="/users" />
                </Col>
                <Col span={20} style={{ margin: "30px 0px" }}>
                    <UserInfo
                        user={user} />
                </Col>
                <Col span={20} style={{ marginBottom: 30 }}>
                    <TodoList
                        todoList={todoList}
                        setTodoList={setTodoList}
                        selectedFilter={selectedFilter}
                        setSelectedFilter={setSelectedFilter} />
                </Col>
            </Row>
        </div>
    )
}

export default TodoPage;