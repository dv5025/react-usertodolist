import React from 'react';
import FilterList from './FilterList';
import { List, Typography, Button } from 'antd';
import { Row, Col } from 'antd';

const TodoList = (prop) => {
    const { todoList, selectedFilter, setTodoList, setSelectedFilter } = prop;

    const markAsDone = (id) => {
        const temp = [...todoList];
        for (let i = 0; i < temp.length; i++) {
            if (temp[i].id == id) {
                temp[i].completed = true;
            }
        }
        setTodoList(temp);
    }

    return (
        <List
            style={{
                backgroundColor: "white"
            }}
            header={
                <Row type="flex" justify="center">
                    <Col span={20}>
                        <h2>Todo List</h2>
                    </Col>
                    <Col span={4}>
                        <Row type="flex" justify="end">
                            <Col>
                                <FilterList
                                    selectedFilter={selectedFilter}
                                    setSelectedFilter={setSelectedFilter} />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            }
            bordered
            dataSource={
                selectedFilter == "All" ?
                    todoList
                    :
                    todoList.filter(item => item.completed == selectedFilter)
            }
            renderItem={(item, index) => (
                <List.Item>
                    <List.Item.Meta
                        description={
                            <div>
                                {
                                    item.completed ?
                                        <Typography.Text delete>[Done]</Typography.Text>
                                        :
                                        <Typography.Text mark>[Doing]</Typography.Text>
                                }
                                &nbsp;
                                {item.title}
                            </div>
                        }
                    />
                    {
                        !item.completed ?
                            <Button onClick={() => { markAsDone(item.id) }} shape="circle" type="danger" icon="check"></Button>
                            :
                            <div></div>
                    }
                </List.Item>
            )}
        />
    );
}

export default TodoList;
