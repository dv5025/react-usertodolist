import React from 'react';
import { Select } from 'antd';

const { Option } = Select;

const FilterList = (props) => {
    const { selectedFilter, setSelectedFilter } = props;

    const handleChange = (value) => {
        setSelectedFilter(value);
    }

    return (
        <div>
            <Select value={selectedFilter} style={{ width: 120 }} onChange={handleChange}>
                <Option value="All">All</Option>
                <Option value={false}>Doing</Option>
                <Option value={true}>Done</Option>
            </Select>
        </div>
    );
}

export default FilterList;
