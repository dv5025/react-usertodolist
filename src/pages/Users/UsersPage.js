import React, { useEffect, useState } from 'react';
import UsersList from './UsersList';
import SearchUser from './SearchUser';
import { Row, Col } from 'antd';

const UserPage = () => {
    const [users, setUsers] = useState([]);
    const [handleKeyword, setHandleKeyword] = useState("");

    useEffect(() => {
        fetchUsers();
    }, []);

    const fetchUsers = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                console.log(data);
                setUsers(data);
            })
            .catch(error => console.log(error));
    };

    return (
        <div>
            <Row type="flex" justify="center">
                <Col>
                    <h1 style={{ marginTop: 30 }}>User Page</h1>
                </Col>
            </Row>
            <Row type="flex" justify="center">
                <Col>
                    <SearchUser
                        users={users}
                        setHandleKeyword={setHandleKeyword} />
                </Col>
            </Row>
            <Row type="flex" justify="center">
                <Col span={20}>
                    <UsersList
                        users={users}
                        handleKeyword={handleKeyword} />
                </Col>
            </Row>
        </div>
    );
}

export default UserPage;
