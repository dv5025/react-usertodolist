import React from 'react';
import { Input } from 'antd';

const SearchUser = (props) => {
    const { Search } = Input;
    const { setHandleKeyword } = props;

    const onKeywordChange = ({ target }) => {
        setHandleKeyword(target.value);
    }

    return (
        <Search
            placeholder="input search text"
            onChange={onKeywordChange}
            style={{ width: 400 }}
            size="large"
        />
    );
}

export default SearchUser;
